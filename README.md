# Encoder multiproposito 
Como parte del kit de hardware abierto para el estudio de fisica este equipo cumple multiples propositos sensando movimientos de rotacion.
El sistema consiste de un encoder recuperado de una impresora HP de chorro de tinta que fue montado con rodamientos en una caja impresa en 3D.
Un arduino nano conectado a los sensores del enconder realiza la adquisicion de los datos que son enviados por el puerto serie a una computadora para su procesamiento.

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Licencia Creative Commons Atribución-NoComercial 4.0 Internacional</a>.
